# PostmarketOS UI Lomiri
## (Re) Packaging Lomiri in PostmarketOS!</b>

There're a lot of TODOs to do, check APKBUILDs of each package.<br/>
For example, converting systemd services into OpenRC ones, or try to enable tests in some packages.<br/>
Maintainer names, package descriptions and package urls are also incorrect. The maintainers that appear there have contributed to several Lomiri packages, but not all of them. In addition, as `postmarketos-ui-lomiri` was discontinued, we should change "Maintainer" to "Contributor".

Lomiri is running via QEMU (PostmarketOS v21.12, Alpine 3.15), but via software rendering.<br/>
QEMU complains that my system is too slow and I have 16GB of RAM available, so probably yes.

I have compiled the UI in stable, as you can see (PostmarketOS v21.12, Alpine 3.15). Lomiri uses some deprecated code in some core packages, so I didn't want to risk building on edge.<br/>
Perhaps building it all again but on edge may be broken due to compiler warnings/errors and deprecated code, but at least we have a recent Lomiri up and running.

## QEMU image

A qemu image is ready to test in the releases folder.

You can also download it here: https://sourceforge.net/projects/repackaging-pmos-ui-lomiri/files/qemu/0.0.1/qemu.zip

Look into qemu folder and execute the bash script (check that the script is executable and you have QEMU installed).<br/>
It runs successfully on Ubuntu 20.04, but it may be better if we build and run it through PostmarketOS qemu, via `pmbootstrap qemu`.<br/>
Check that pmbootstrap is using the v21.12 branch, and not the edge one (run `pmbootstrap init` to select branch).

## Rootfs image

A rootfs is ready to test in the releases folder.

You can also download it here: https://sourceforge.net/projects/repackaging-pmos-ui-lomiri/files/rootfs/0.0.1/rootfs-lomiri-amd64-alpine3.15.tar.gz

This is ideal for testing Lomiri on real hardware (dual-boot). You need to have a `40_custom` grub file in `/etc/grub.d/40_custom`. If not, create it and put the following lines:

```
#!/bin/sh
# /etc/grub.d/40_custom
exec tail -n +3 $0
# This file provides an easy way to add custom menu entries.  Simply type the
# menu entries you want to add after this comment.  Be careful not to change
# the 'exec tail' line above.

# Alpine Linux is in /dev/sda4 (MSDOS partition type) -> (hd0,msdos4)
# For GPT partition should be: (hd0,gpt4)

# Find UUID of your partitions with `sudo blkid`
# You can also replace root=UUID=THE-UUID-OF-THE-CREATED-EXT4-PARTITION for root=/dev/sda4. However, it is less reliable.

menuentry "Alpine Linux 3.15" {
  set root='(hd0,msdos4)'
  linux /boot/vmlinuz-lts root=UUID=THE-UUID-OF-THE-CREATED-EXT4-PARTITION modules=sd-mod,usb-storage,ext3,ext4 quiet
  initrd /boot/initramfs-lts
}
```

Read the code carefully. The comments written in there may be useful for you.

You will also need to create a new EXT4 blank partition in your hard disk, mount it in `/mnt` and decompress the downloaded rootfs file there.

After that, in your main OS, you have to run `sudo update-grub` to apply all changes.

If all goes well, the option for booting into Alpine Linux will be displayed (run the custom option "Alpine Linux 3.15"! The option that GRUB auto-detects does not work!).

### Instructions

When the image has booted, please follow the instructions here: https://wiki.postmarketos.org/wiki/Lomiri

Lomiri can take a few minutes to run, and it's slow, so be patient.

#### Notes (QEMU only)

Tip: You can tweak the QEMU video settings in the script `boot-qemu.sh`.<br/>
By default, the parameter we're looking for is `video=1920x1080@60`. Put your desired resolution there, e.g.: `video=1280x720@60`

Recommendation: If QEMU does not work for you, please try in a lower resolution (by tweaking the video parameter). Ideally, try in real x86_64 (64-bit) hardware.

## Video

There's also a video available showing Lomiri in QEMU in media folder.

![Lomiri showcase video QEMU](https://gitlab.com/antoni.aloytorrens/repackaging-postmarketos-ui-lomiri/-/raw/master/media/video/Lomiri-video.mp4)

## Photo

![Lomiri showcase image QEMU](https://gitlab.com/antoni.aloytorrens/repackaging-postmarketos-ui-lomiri/-/raw/master/media/image/Lomiri-first-working-draft-real-hardware.jpg)

With GPU acceleration the performance is increased. However, it may also take some minutes to load due to missing running services, applications, etc.

### Hardware specifications

The hardware is quite old and has a HDD disk, but Lomiri performs quite well anyways.

![Hardware specifications](https://gitlab.com/antoni.aloytorrens/repackaging-postmarketos-ui-lomiri/-/raw/master/media/image/HP-ProLiant-MicroServer-Neofetch.png)

## FAQ

- Q: The package manager `apk` throws me an error about `provides $package` or `overrides $package` and it does not let me continue building!

<b>A: You may need to `pmbootstrap build --force $package` the packages from backports manually. Then try again.</b>

--

Antoni Aloy Torrens
