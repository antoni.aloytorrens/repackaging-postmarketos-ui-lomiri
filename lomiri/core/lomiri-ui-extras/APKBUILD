# Maintainer: Antoni Aloy <aaloytorrens@gmail.com>
pkgname=lomiri-ui-extras
pkgver=0.5_git20220308
_commit="8ad53d43bb038a239a14a9f7090060bc1b2b0c1e"
pkgrel=0
pkgdesc="Repository of QML components that do not have the necessary level of quality for inclusion in the toolkit"
arch="all"
url="https://unity8.io"
license="LGPL-3.0-only"
makedepends="cmake cmake-extras gettext-dev cups-dev exiv2-dev lomiri-ui-toolkit-dev qt5-qtbase-dev qt5-qttools-dev qt5-qtdeclarative-dev"
checkdepends="diffutils bash grep xvfb-run dbus-test-runner"
source="https://gitlab.com/ubports/core/lomiri-ui-extras/-/archive/$_commit/lomiri-ui-extras-$_commit.tar.gz"
subpackages="$pkgname-lang"
options="!check" # several tests fail
builddir="$srcdir/$pkgname-$_commit"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
        cmake -B build \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		$CMAKE_CROSSOPTS .
	cmake --build build
}

check() {
	xvfb-run make check
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
6d70874008d5b1d898b9ae13e2ef2a49e5bea656f1c88142a3f6e4abc006276532d69604d34d78093f7440f338196c39042e0aee42280a7860623d3f1d38c3da  lomiri-ui-extras-8ad53d43bb038a239a14a9f7090060bc1b2b0c1e.tar.gz
"
