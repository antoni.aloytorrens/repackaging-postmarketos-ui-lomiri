# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Luca Weiss <luca@z3ntu.xyz>
pkgname=qtmir
pkgver=0_git20211204
_commit="cde69ffa84d1e8172f151765a782340a6b5d7381"
pkgrel=0
pkgdesc="QPA plugin to make Qt a Mir server"
arch="all"
url="https://gitlab.com/ubports/core/qtmir"
license="LGPL-3.0-only"
# content-hub-dev
makedepends="cmake cmake-extras gtest-dev lomiri-mir-dev process-cpp-dev qt5-qtdeclarative-dev qt5-qtsensors-dev lomiri-app-launch-dev gsettings-qt-dev valgrind-dev lomiri-url-dispatcher-dev libxrender-dev xkbcomp-dev cgmanager-dev lttng-ust-tools"
depends="libqtdbustest libqtdbusmock"
checkdepends="py3-dbusmock dbus"
source="https://gitlab.com/ubports/core/qtmir/-/archive/$_commit/qtmir-$_commit.tar.gz
	0001-Replace-usage-of-GTEST_DISALLOW_ASSIGN_.patch
	"
subpackages="$pkgname-dev"
builddir="$srcdir/$pkgname-$_commit"
options="!check" # Tests fail under QEMU

# NO_TESTS=ON -> mirtest not found
build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DWITH_CONTENTHUB=OFF \
		-DNO_TESTS=ON
	make -C build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make -C build DESTDIR="$pkgdir" install
}

sha512sums="
f4c525ca9b2616c57c5c15f3c7203a95ecbcdcda161d421e0d81f5d13beab88b3854da05a09b32da34324454b2f3c7d567ac606ba894c6b693504e8f1167f18f  qtmir-cde69ffa84d1e8172f151765a782340a6b5d7381.tar.gz
e45a59f8c3a1fa59e0d198db960486bff90db28381f906fa4746d775786e56087237b105e6463a72e31f3525ceb941952b31390d52a179da87a97e2b13c4c074  0001-Replace-usage-of-GTEST_DISALLOW_ASSIGN_.patch
"
